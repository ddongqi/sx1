import java.util.Scanner;
	public class Main {
     public static void main(String[] args){
	
//输入玩家个数 游戏局数
		System.out.println("请输入大于10的游戏人数:");
		Scanner reader=new Scanner(System.in);  
		int N=reader.nextInt();
		System.out.println("游戏局数:");
		int b=reader.nextInt();
        Scanner scanner=new Scanner(System.in);
        double sum;
        double max,min;
        int maxzf=0;
        int minzf=0;
        int []zf=new int[100];   
        double []v=new double[100];
        int [][]grade=new int[100][100];
		
		
        for(int j=1;j<=b;j++)
        { 
            sum=0.0;
            max=0.0;
            System.out.println("第"+j+"轮游戏");
            for(int i=1;i<=N;i++)
            {
                System.out.print("请玩家"+i+"输入数字：");
                double num=scanner.nextDouble();//玩家给出数字
				while(num>=100||num<0){
				System.out.println("请重新输入");
				 num=scanner.nextDouble();
				}
				
                v[i]=num;
                sum=sum+num;//所有玩家的总分
            }
            double G=(sum/N)*0.618;//计算G值
			System.out.println("G="+G);
            min=Math.abs(v[1]-G);//MATH.ABS绝对值
 //跟G比较,最近最远
            for(int I=1;I<=N;I++)                         
            {
                if(Math.abs(v[I]-G)>=max)
                {
                    max=Math.abs(v[I]-G);
                }
            
                if(Math.abs(v[I]-G)<=min)
                {
                    min=Math.abs(v[I]-G);    
                }
                    
            }
//将得分放到适当的位置			
            for(int m=1;m<=N;m++)
            {
                if(Math.abs(v[m]-G)==max)
                {
                    grade[j][m]=-2;
                }
                else if(Math.abs(v[m]-G)==min)
                {
                    grade[j][m]=N;
                }
               else 
                {
                    grade[j][m]=0;
                }
				 System.out.println("第"+m+"名游戏玩家所得分数为："+grade[j][m]);
            }
        }
		
		
		
//求总分	
		
        for(int p=1;p<=N;p++)
        {
            for(int q=1;q<=b;q++)
            {
                zf[p]=grade[q][p]+zf[p];
            }
			System.out.println("第"+p+"名玩家所得总分为："+zf[p]);
        }
		
//求最大值最小值	
        for(int s=1;s<=N;s++)
        {
            if(zf[s]>=maxzf)
            {
                maxzf=zf[s];
            }
            if(zf[s]<=minzf)
            {
                minzf=zf[s];
            }
        }
//最大值最小值赋值输出		
        for(int t=1;t<=N;t++)
        {
            if(zf[t]==maxzf)
            {
                System.out.println("玩家"+t+"胜利！");
            }
            if(zf[t]==minzf)
            {
                System.out.println("玩家"+t+"失败！");
            }
        }
		
    }
}